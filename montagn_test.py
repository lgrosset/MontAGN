# -*- coding: utf-8 -*-
import numpy as np
import scipy.integrate as sci
import time
import tarfile
import os

import montagn
import montagn_utils as mu
import montagn_classes as mc
import montagn_polar as mpol
import matplotlib.pyplot as plt
import gui_random as g_rand
import montagn_output as mout
import montagn_launch as ml
import matplotlib.pyplot as plt
import random as rand
import gui_random as g_rand
try:
    import patiencebar as g_pgb
except:
    print 'For a better use you can install patiencebar (work with pip)'
    import gui_progbar as g_pgb
import time



def test_env(n,choix,npoly,g,nang=999,outname=''):
    """
    Routine launching n time the subroutine computing the scattering angles from envelope
    Display the phase functions of alpha and beta
    """
    genseed=g_rand.gen_generator()
    alpha_tab = np.linspace(0.,0.,nang)
    progbar=g_pgb.Patiencebar(valmax=n,up_every=1)
    for i in range(n):
        progbar.update()
        mu=mpol.SimuEnv(choix,g,npoly,genseed)
        
        alpha=np.arccos(mu)
        alpha_tab[int(round(alpha/np.pi*(nang-1)))]+=1

        if((i)%(n/10)==0 and outname!=''):
            f = open('Output/'+outname+'_env.dat','w')
            f.write("%f\n"%(i+1))
            for j in xrange(nang):
                f.write("%f\t"%alpha_tab[j])
            f.close()

    if(outname!=''):
        f = open('Output/'+outname+'_env.dat','w')
        f.write("%f\n"%(i+1))
        for j in xrange(nang):
            f.write("%f\t"%alpha_tab[j])
        f.close()
    mpol.plotphase_xfix(alpha_tab*nang/float(n),title='Envelope probability density function')
    mpol.plotphase_xfix(alpha_tab*nang/float(n),normsin=1,title='Envelope phase function')



def test_scat(n,r,wvl,grain,nang=999,QU=[0.0,0.0],force_alpha=[],outname=''):
    """
    Routine launching n time the subroutine computing the scattering angles
    Display the phase functions of alpha and beta
    """
    genseed=g_rand.gen_generator()
    photon=mc.Photon(wvl,0,0,'test',0,0,0)
    photon.Ss=[[1.0],[QU[0]],[QU[1]],[0.0]]
    alpha_tab = np.linspace(0.,0.,nang)
    beta_tab = np.zeros([nang,5])
    progbar=g_pgb.Patiencebar(valmax=n,up_every=1)
    eff=[0,0]
    x=2*np.pi*r/wvl
    print 'x : ',x
    for i in range(n):
        progbar.update()
        alpha, beta , eff= mpol.diffpolar(r,wvl,photon,grain,genseed,eff_mes=eff,force_alpha=force_alpha)
        photon.Ss=[[1.0],[QU[0]],[QU[1]],[0.0]]
        
        alpha_tab[int(round(alpha/np.pi*(len(grain.phaseNorm[:,0,0])-1)))]+=1
        beta_tab[int(round(beta/np.pi*0.5*(len(grain.phaseNorm[:,0,0])-1))),min(photon.interact,4)]+=1


        if((i)%(n/10)==0 and outname!=''):
            f = open('Output/'+outname+'_scatfunc.dat','w')
            f.write("%f\n"%(i+1))
            for j in xrange(nang):
                f.write("%f\t"%alpha_tab[j])
            f.write("\n")
            for k in range(5):
                for j in xrange(nang):
                    f.write("%f\t"%beta_tab[j,k])
                f.write("\n")
            f.close()

    print 'efficiency of von Neumann rejection : ',float(eff[0])/float(eff[1])*100,'%'
    print '(with ',eff[0],' positive results for ',eff[1],'tries)'
    if(outname!=''):
        f = open('Output/'+outname+'_scatfunc.dat','w')
        f.write("%f\n"%(i+1))
        for j in xrange(nang):
            f.write("%f\t"%alpha_tab[j])
        f.write("\n")
        for k in range(5):
            for j in xrange(nang):
                f.write("%f\t"%beta_tab[j,k])
            f.write("\n")
        f.close()
    mpol.plotphase_xfix(alpha_tab*nang/float(n),title='Alpha probability density function x = '+str(x))
    mpol.plotphase_xfix(alpha_tab*nang/float(n),normsin=1,title='Alpha phase function x = '+str(x))
    #for i in range(5):
    i=0
    mpol.plotphase_xfix(beta_tab[:,i]*nang/float(n),twopi=1,title='Beta probability density function after '+str(i+1)+' scatterings')


def test_T():
    return 0

def test_intesect():
    return 0

def test_loginterp(n):
    vect=[10,100,1000,1e4,1e5,1e6,1e7]
    x=np.linspace(0,6,7)+1
    x0=np.linspace(0+6./n,6-6./n,n-2)+1
    ylin=[]
    ylog=[]
    start=time.time()
    for i in range(n-2):
        ylog.append(mpol.loginterp(x,vect,x0[i]))
    duree=(time.time() - start)
    print 'Time spend in log interpolation : ',duree,' s'
    start=time.time()
    for i in range(n-2):
        ylin.append(np.interp(x0[i],x,vect))
    duree=(time.time() - start)
    print 'Time spend in linear interpolation : ',duree,' s'
    #print 'Results : log : y = ',ylog,' and lin : y = ',ylin
    mpol.plot_1D(x,vect,logy=1,n=0,symbol='k',label='data')
    mpol.plot_1D(x0,ylog,n=0,symbol='r+',label='log')
    mpol.plot_1D(x0,ylin,n=0,symbol='b+',label='lin',title='Test of the loginterp module',xlabel='x',ylabel='y')

def test_indice(n):
    x=np.linspace(0,10,11)
    x0=np.linspace(-1,11,n)
    indice=[]
    start=time.time()
    for i in range(n):
        indice.append(mpol.get_indice(x,x0[i]))
    duree=(time.time() - start)
    print 'Time spend in indice finding : ',duree,' s'
    print 'or : ',duree/n,' s/indice'
    mpol.plot_1D(x,x,n=0,symbol='k',label='theory')
    mpol.plot_1D(x0,x[indice],n=0,symbol='r+',label='indice')
    mpol.plot_1D(x0,indice,n=0,symbol='b+',label='indice')

def test_indice_short(n):
    x=np.linspace(0,2,3)
    x0=np.linspace(-1,3,n)
    indice=[]
    indicealt=[]
    indicealtalt=[]
    start=time.time()
    for i in range(n):
        indice.append(mpol.get_indice(x,x0[i]))
    duree=(time.time() - start)
    print 'Time spend in indice finding : ',duree,' s'
    print 'or : ',duree/n,' s/indice'
    start=time.time()
    for i in range(n):
        indicealt.append(mpol.get_indice_alt(x,x0[i]))
    duree=(time.time() - start)
    print 'Time spend in alt indice finding : ',duree,' s'
    print 'or : ',duree/n,' s/indice'
    start=time.time()
    for i in range(n):
        indicealtalt.append(mpol.get_indice_altalt(x,x0[i]))
    duree=(time.time() - start)
    print 'Time spend in alt indice finding : ',duree,' s'
    print 'or : ',duree/n,' s/indice'
    mpol.plot_1D(x,x,n=0,symbol='k',label='theory')
    mpol.plot_1D(x0,x[indice],n=0,symbol='r+',label='value')
    mpol.plot_1D(x0,indice,n=0,symbol='r',label='indice')
    mpol.plot_1D(x0,x[indicealt],n=0,symbol='b+',label='alt value')
    mpol.plot_1D(x0,indicealt,n=0,symbol='b',label='alt indice')
    mpol.plot_1D(x0,x[indicealtalt],n=0,symbol='g+',label='altalt value')
    mpol.plot_1D(x0,indicealtalt,n=0,symbol='g',label='altalt indice')

def test_pah_cc(n=1000):
    wvl,cc=mu.read_CC_PAH()
    pah=mc.Grain('pah',0,mu.grain_size_surf(1,10,10)[0],1,1,1,1,1,1,1,0)
    pah.CC_wvl=wvl
    pah.CC=cc
    cst=mc.Constant()
    #wvl=np.ones([n])*0.08
    #step=np.log(30-0.08)/(n-1.)
    #for i in range(n):
    #    wvl[i]+=+np.exp(step*(i))
    wvl=np.logspace(np.log10(0.08),np.log10(30),n)
    wvl=wvl*1e-6
    K=np.zeros([n])
    for i in range(n):
        K[i]=mu.KPAH(pah,wvl[i])
    mpol.plot_1D(wvl,K,title='pah cross section',logx=1,logy=1,xlabel='wvl (m)',ylabel='cross section (m2)')


#isrf=mu.read_isrf(d=8)

#jnu=mpol.read_Jnu_Draine()
