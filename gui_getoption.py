# Written by Guillaume, 2014
# reference: http://www.tutorialspoint.com/python/python_command_line_arguments.htm

"""
This module wraps getopt library to return a convenient object whose properties are lists containing the attributes and options provided in the command-line call.
"""

from getopt import getopt as _getoptgetopt

class _font:
    red = '\033[31m'
    blue = '\033[34m'
    normal = '\033[39m'


def _aslist(data, numpy=False, integer=False):
    if not hasattr(data, "__iter__"): return [data]
    if len(data)==1 and not isinstance(data, list): return [data]
    return list(data)

class CallError(Exception):
    def __init__(self, cline=""):
        self.message = "Could not parse the command-line properly"
        self.args = [cline]

class getoption:
    """
    Wraps getopt library to return a convenient parser-object to be called with a command-line as input parameter

    Args:
      * opt (string): the concatenation of all authorized one-char options i.e. "-e"
      * optval (string): the concatenation of all authorized one-char options with input value i.e. "-e 3"
      * param (list of string): the list of all authorized multi-char options i.e. "--check"
      * paramval (list of string): the list of all authorized multi-char options with input value i.e. "--name=test"
      * man (string): the manual page of the script
      * sysargv (list of string) optionnal: the command-line call, as it is provided by sys.argv

    Returns:
      A python object to be called with a command-line as input parameter
    
    Raises:
      CallError: if the getopt library cannot parse properly the command-line provided

    Note:        
        The returned object possesses as attributes:
            * ``command``: a string that shows the full command-line
            * ``nargs``: an integer showing the total number of arguments provided
            * ``optkey``: a list of all options provided in the command-line call, in the order of the command-line call
            * ``opt``: a dictionnary of the option provided and their associated value. Value-less options return True
            * ``attrs``: a list of all the attributes provided after the options
            * ``man``: a string that contains the manual of how to use this script
    
    >>> import getoption, sys
    >>> scriptcall = getoption.getoption(sys.argv, opt='wes', optval='r',
        man='This is the manual page. Thanks you for using getoption')
    >>> if scriptcall['r'] is not None: print "r option was given"
    >>> if 'h' in scriptcall.optkey:
    >>>     print scriptcall.man
    >>>     sys.exit()

    Note:
      * outputObject['option_name'] will return the value of the option ``option_name`` (or ``True`` is the option has no value), or ``None`` if the ``option_name`` was not provided in the command line call
      * The option ``-h`` is automatically added to the list of one-char authorized options
      * The syntax of the script is automatically generated from authorized input given to getoption and added at the beginning of the provided manual ``man``

    >>> from getoption import getoption
    >>> thecall = "conex -w --logfile=~/log.txt 1 TP 234"
    >>> # the .split(' ') below is used to emulate the shape of sys.argv
    >>> scriptcall = getoption(thecall.split(' '), opt='wes', paramval=['logfile'],
        man='Manual page of that script')
    >>> scriptcall. <tab>
    a.attrs    a.command  a.man      a.nargs    a.opt      a.optkey
    >>> print scriptcall.nargs
    5
    >>> print scriptcall.optkey
    ['w', 'logfile']
    >>> print scriptcall.attrs
    ['1', 'TP', '234']
    >>> print scriptcall['w']
    True
    >>> print scriptcall['logfile']
    ~/log.txt
    >>> print scriptcall['e']
    None
    """
    def __init__(self, opt='', optval='', param=[], paramval=[], man="", sysargv=None):
        param = _aslist(param)
        paramval = _aslist(paramval)
        self._shortOpts = opt + 'h' + ''.join([i+':' for i in optval])
        self._longOpts = param + [i+'=' for i in paramval]
        if sysargv is not None:
            ficname = str(sysargv[0])
        else:
            ficname = "file"
        self._man = 'Syntax:\n' + _font.red + ficname + _font.blue + "".join([' -'+str(i) for i in opt]) + _font.blue.join([' -'+str(i)+ " "+_font.normal+"<val>" for i in optval]) + _font.blue + "".join([' --'+str(i) for i in param]) + _font.blue.join([' --'+str(i)+_font.normal+"=<val>" for i in paramval]) + _font.normal
        self.man = self._man + (man!="")*"\n\n" + man
        self._callit(sysargv)

    def _callit(self, sysargv):
        self.opt = {}
        self.optkey = []
        if sysargv is None:
            self.nargs = 0
            self.command = ""
            return
        self.nargs = len(sysargv[1:])
        self.command = ' '.join(sysargv)
        try:
            opt, self.attrs = _getoptgetopt(sysargv[1:], self._shortOpts, self._longOpts)
        except:
            print self._man+"\n"
            raise CallError(self.command)
        for element in opt:
            self.optkey.append(element[0].replace('-',''))
            if element[1]=='':
                self.opt.update({element[0].replace('-',''): True})
            else:
                self.opt.update({element[0].replace('-',''): element[1]})


    def __getitem__(self, key):
        if self.opt.has_key(key):
            return self.opt[key]
        else: return None

    def __call__(self, sysargv):
        self._callit(sysargv)
        

