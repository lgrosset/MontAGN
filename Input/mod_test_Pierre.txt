#dust silicates silicates rgmin rgmax alpha rsub
dust silicates silicates 0.005e-6 0.25e-6 -3.5 0.3pc
dust graphites_ortho graphites_ortho 0.005e-6 0.25e-6 -3.5 0.3pc
dust graphites_para graphites_para 0.005e-6 0.25e-6 -3.5 0.3pc
dust electrons electrons 0.5e-9 0.5e-9 0 0
AGN_simple [silicates,graphites_ortho,graphites_para,electrons] [0.0519194,0.0571114,0.0285557,0] [0,0,0,0] [0,0,0,1.0e10] 3pc 3pc 30 25
#AGN_simple [silicates,graphites_ortho,graphites_para] [0.0040348,0.00443829,0.00221915] [0,0,0] [0,0,0] 25pc 25pc 30 25
res_map 0.3pc
rmax_map 3pc
source AGN spectre_agn.dat 1e67 'default'
af 0.
enpaq 1e67
