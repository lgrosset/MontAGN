#dust silicates silicates rgmin rgmax alpha rsub
dust pah_neu pah_neutral 0.0005e-6 0.005e-6 -3.5 0.05pc
#dust pah_ion pah_ionised 0.001e-6 0.01e-6 -3.5 0.05pc
torus_const pah_neu 10pc 10 0 0
#torus_const pah_ion 10pc 0.1 0 0
res_map 0.5pc
rmax_map 25pc
source AGN spectre_flat.dat 1e36 0 0 0 0 0 0
af 0.
enpaq 1e36
