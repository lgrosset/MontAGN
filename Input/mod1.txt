#dust silicates silicates rgmin rgmax alpha rsub
dust silicates silicates 0.005e-6 0.25e-6 -3.5 0.05AU
spherepower [silicates] [5800.0] 0 1.0AU
res_map 1AU
rmax_map 25AU
source star spectre_NIRpeak.dat 3.846e26 'default'
af 0.
enpaq 3.846e26
