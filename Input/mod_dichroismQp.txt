#dust silicates silicates rgmin rgmax alpha rsub
dust graphites_ortho graphites_ortho 0.005e-6 0.25e-6 -3.5 0.00pc
#dust graphites_para graphites_para 0.005e-6 0.25e-6 -3.5 0.00pc
#cylinder [graphites_ortho] [0.061] 5.pc 5.pc #tauV=2.5
#cylinder [graphites_ortho] [0.122] 5.pc 5.pc #tauV=5
#cylinder [graphites_ortho] [0.244] 5.pc 5.pc #tauV=10
#cylinder [graphites_ortho] [0.488] 5.pc 5.pc #tauV=20
#cylinder [graphites_ortho] [1.22] 5.pc 5.pc #tauV=50
cylinder [graphites_ortho] [2.44] 5.pc 5.pc #tauV=100
res_map 0.5pc
rmax_map 10pc
source AGN spectre_flat_NIR.dat 1e36 emission1
emission emission1 emdir1 polardir1 polar1
emdir emdir1 90. 0.
polardir polardir1 0.
polar polar1 0.999 0 0
af 0.
enpaq 1e36
