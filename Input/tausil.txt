#dust silicates silicates rgmin rgmax alpha rsub
#5.43e4 for tauH=1
dust silicates silicates 0.005e-6 0.25e-6 -3.5 0.0pc
cylinder silicates 20AU 20AU 5.43e4
res_map 4AU
rmax_map 80AU
source star spectre_flat.dat 3.846e26 0. 0. 0.999 0.999 0. 0.
af 0.
enpaq 3.846e26
