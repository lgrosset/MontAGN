import numpy as np

from time import time as timetime
from multiprocessing import current_process as multiprocessingcurrent_process
sysmaxint = 2**32

def gen_seed():
    """
    Generates a seed based on the micro second time and the process id. This makes it multi-threading safe.

    Returns:
      an integer seed

    >>> print gen_seeed()
    1825296981
    """
    return int(long(float(str((timetime()%3600)/3600)[2:]))*multiprocessingcurrent_process().pid%sysmaxint)



def gen_generator(seed=None):
    """
    Generates a random generator with a seed based on the micro second time and the process id. This makes it multi-threading safe.

    Returns:
      a random number generator object

    >>> rnd = gen_generator()
    >>> print rnd.uniform()
    0.472645

    Note:
      This function calls `gen_seed` if seed is not given.
    """
    if seed is None: return np.random.RandomState(gen_seed())
    return np.random.RandomState(seed)

