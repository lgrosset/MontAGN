# Written by Guillaume, 2013, 2015

import os as _os
from sys import stdout as _stdout
#from Queue import Queue as _Queue
from threading import Thread as _Thread
from time import sleep as _sleep
from multiprocessing import Manager as _Manager

"""
import time
def worker(pbm):
    time.sleep(np.random.random()*4)
    pbm.update()
def worker2(pbm):
    time.sleep(np.random.random()*4)
    pbm[0].update()

pbm = Progbarmulti()
pbm.reset(10, 30, "mouhaha")
for i in range(10):
    ttt = _Thread(target=worker, args=(pbm,))
    ttt.daemon = True
    ttt.start()

from multiprocessing import Pool
threads=4
num=10
pbm = Progbarmulti()
pbm.reset(num, 30, "mouhaha")
pool = Pool(processes=threads)
result = pool.map_async(worker2, ((pbm,) for i in range(num)))
pool.close()
pool.join()
result = result.get()

"""


class Patiencebar(object):
    """Progress bar
    *valmax* is the last value of the iteration. Default is 100.
    *barsize* is the size of the bar in the opened window. If empty, the bar will automatically fit the window
    *title* is the title."""
    def __init__(self, valmax=100, barsize=None, title=None, bar=True, up_every=2):
        self.reset(valmax=valmax, barsize=barsize, title=title, bar=bar, up_every=up_every)

    @property
    def valmax(self):
        return self._valmax
    @valmax.setter
    def valmax(self, value):
        raise AttributeError, "Read-only"

    @property
    def barsize(self):
        return self._barsize
    @barsize.setter
    def barsize(self, value):
        raise AttributeError, "Read-only"

    @property
    def title(self):
        return self._title
    @title.setter
    def title(self, value):
        raise AttributeError, "Read-only"

    @property
    def bar(self):
        return self._bar
    @bar.setter
    def bar(self, value):
        raise AttributeError, "Read-only"

    @property
    def up_every(self):
        return self._up_every
    @up_every.setter
    def up_every(self, value):
        raise AttributeError, "Read-only"    
    

    def reset(self, valmax=None, barsize=None, title=None, bar=None, up_every=None):
        """Progress bar
        *valmax* is the last value of the iteration. Default is 100.
        *barsize* is the size of the bar in the opened window. If empty, the bar will automatically fit the window
        *title* is the title."""       
        #gets terminal size
        def ioctl_GWINSZ(fd):
            try:
                import fcntl, termi_os, struct
                cr = struct.unpack('hh', fcntl.ioctl(fd, termi_os.TIOCGWINSZ,
            '1234'))
            except:
                return
            return cr
        cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
        if not cr:
            try:
                fd = _os.open(_os.ctermid(), _os.O_RDONLY)
                cr = ioctl_GWINSZ(fd)
                _os.cl_ose(fd)
            except:
                pass
        if not cr: cr = (_os.environ.get('LINES', 25), _os.environ.get('COLUMNS', 80))

        #initialize
        self.win_width = int(cr[1])
        self.step = 0
        self._nextup = 0
        self.in_progress = False
        if valmax is not None:
            self._valmax = float(valmax)
        else:
            self._valmax = float(getattr(self, 'valmax', 100))
        if barsize is not None:
            self._barsize = int(barsize)
        else:
            self._barsize = float(getattr(self, 'barsize', self.win_width-8))
        if title is not None:
            self._title = str(title)
        elif getattr(self, 'title', None) is not None:
            self._title = str(getattr(self, 'title'))
        else:
            self._title = None
        if bar is not None:
            self._bar = bool(bar)
        else:
            self._bar = bool(getattr(self, 'bar', True))
        if up_every is not None:
            self._up_every = int(min(100,max(0,up_every)))
        else:
            self._up_every = int(getattr(self, 'up_every', 2))

    
    def update(self, step=None):
        """Calling .update() adds 1 to the progress of the bar.
        Calling .update(i) puts the progress of the bar to i value."""
        self._doupdate(step=step)
        
    def _doupdate(self, step=None):
        if step is None: # no step given
            if self.bar is True: # if display progressbar
                self.step += 1
            else: # if pure text
                self.step = 'tic'
        else: # step given
            if self.bar is True: # if display progressbar
                self.step = min(step, self.valmax)
            else: # if pure text
                self.step = str(step)
        if self.bar is True: # if display progressbar
            perc = int(self.step*100./self.valmax)
            if perc < self._nextup: return
            self._nextup = min(100, perc+self._up_every)
            bar = int(perc/100.*self.barsize)
      
            # render
            if self.in_progress is False:
                if self.title is not None: print(self.title)
                self.in_progress = True
            spacing = (self.win_width-self.barsize-8)/2.
            out = '\r%s[%s%s] %3d %%%s' % (' ' * int(spacing), '=' * bar, ' ' * int(self.barsize - bar), perc, ' ' * int(spacing+0.5))
            _stdout.write(out)
            _stdout.flush()
        else: # if pure text
            print(self.step)
        if perc==100: print("\r")


class Progbarmulti(Patiencebar):
    def __init__(self, valmax=100, barsize=None, title=None, bar=True):
        super(Progbarmulti, self).__init__(self, valmax=valmax, barsize=barsize, title=title, bar=bar)
        self._q = _Manager().Queue(maxsize=0)
        self._running = True
        checker = _Thread(target=self._check)
        checker.setDaemon(True)
        checker.start()

    @property
    def running(self):
        return self._running
    @running.setter
    def running(self, value):
        raise AttributeError, "Read-only"
    
    def start(self):
        self._running = True

    def stop(self):
        self._running = False
 
    def reset(self, valmax=None, barsize=None, title=None, bar=None):
        super(Progbarmulti, self).reset(self, valmax=valmax, barsize=barsize, title=title, bar=bar)
        self.start()

    def _check(self):
        while self._running and self.step<self.valmax:
            step = self._q.get()
            self._doupdate(step=step)
            self._q.task_done()
            _sleep(0.1)
        self.stop()

    def update(self, step=None):
        self._q.put(step)
