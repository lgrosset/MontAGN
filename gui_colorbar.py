
import matplotlib.cm as _cm
from matplotlib.colors import LinearSegmentedColormap as _matplotlibcolorsLinearSegmentedColormap
from matplotlib.pyplot import Normalize as _matplotlibpyplotNormalize

_colormap_looping = {  'red'  :  (  (0., 0., 0.),
                                    (1/6., 1., 1.),
                                    (2/6., 1., 1.),
                                    (3/6., 0., 0.),
                                    (4/6., 0., 0.),
                                    (5/6., 0., 0.),
                                    (1., 0., 0.)),
                      'green':  (   (0., 0., 0.),
                                    (1/6., 0., 0.),
                                    (2/6., 1., 1.),
                                    (3/6., 1., 1.),
                                    (4/6., 1., 1.),
                                    (5/6., 0., 0.),
                                    (1., 0., 0.)),
                      'blue' :  (   (0., 0., 0.),
                                    (1/6., 0., 0.),
                                    (2/6., 0., 0.),
                                    (3/6., 0., 0.),
                                    (4/6., 1., 1.),
                                    (5/6., 1., 1.),
                                    (1., 0., 0.))}
cm_looping = _matplotlibcolorsLinearSegmentedColormap('looping', _colormap_looping, 1024)
_additional_cm={'looping':cm_looping}

_colormap_looping_mask = {  'red'  :  (  (0., 0., 0.),
                                         (1/7., 0.75, 0.75),
                                         (2/7., 0.75, 0.75),
                                         (3/7., 0.75, 0.75),
                                         (4/7., 0., 0.),
                                         (5/7., 0., 0.),
                                         (6/7., 0., 0.),
                                         (1., 0.75, 0.75)),
                            'green':  (   (0., 0., 0.),
                                          (1/7., 0.75, 0.75),
                                          (2/7., 0., 0.),
                                          (3/7., 0.75, 0.75),
                                          (4/7., 0.75, 0.75),
                                          (5/7., 0.75, 0.75),
                                          (6/7., 0., 0.),
                                          (1., 0.75, 0.75)),
                            'blue' :  (   (0., 0., 0.),
                                          (1/7., 0.75, 0.75),
                                          (2/7., 0., 0.),
                                          (3/7., 0., 0.),
                                          (4/7., 0., 0.),
                                          (5/7., 0.75, 0.75),
                                          (6/7., 0.75, 0.75),
                                          (1., 0.75, 0.75))}
cm_loopingmask = _matplotlibcolorsLinearSegmentedColormap('loopingmask', _colormap_looping_mask, 1024)
_additional_cm={'loopingmask':cm_loopingmask}

#_gist_earth = {     'red'  : ((0.0, 0.0, 0.0000),
#                              (0.2824, 0.1882, 0.1882),
#                              (0.4588, 0.2714, 0.2714),
#                              (0.5490, 0.4719, 0.4719),
#                              (0.6980, 0.7176, 0.7176),
#                              (0.7882, 0.7553, 0.7553),
#                              (1.0000, 0.9922, 0.9922)),
#                    'green': ((0.0, 0.0, 0.0000),
#                              (0.0275, 0.0000, 0.0000),
#                              (0.1098, 0.1893, 0.1893),
#                              (0.1647, 0.3035, 0.3035),
#                              (0.2078, 0.3841, 0.3841),
#                              (0.2824, 0.5020, 0.5020),
#                              (0.5216, 0.6397, 0.6397),
#                              (0.6980, 0.7171, 0.7171),
#                              (0.7882, 0.6392, 0.6392),
#                              (0.7922, 0.6413, 0.6413),
#                              (0.8000, 0.6447, 0.6447),
#                              (0.8078, 0.6481, 0.6481),
#                              (0.8157, 0.6549, 0.6549),
#                              (0.8667, 0.6991, 0.6991),
#                              (0.8745, 0.7103, 0.7103),
#                              (0.8824, 0.7216, 0.7216),
#                              (0.8902, 0.7323, 0.7323),
#                              (0.8980, 0.7430, 0.7430),
#                              (0.9412, 0.8275, 0.8275),
#                              (0.9569, 0.8635, 0.8635),
#                              (0.9647, 0.8816, 0.8816),
#                              (0.9961, 0.9733, 0.9733),
#                              (1.0000, 0.9843, 0.9843)), 
#                    'blue' : ((0.0, 0.0, 0.0000),
#                              (0.0039, 0.1684, 0.1684),
#                              (0.0078, 0.2212, 0.2212),
#                              (0.0275, 0.4329, 0.4329),
#                              (0.0314, 0.4549, 0.4549),
#                              (0.2824, 0.5004, 0.5004),
#                              (0.4667, 0.2748, 0.2748),
#                              (0.5451, 0.3205, 0.3205),
#                              (0.7843, 0.3961, 0.3961),
#                              (0.8941, 0.6651, 0.6651),
#                              (1.0000, 0.9843, 0.9843))}


_gist_earth_loop = {'red'  : ((0.0, 0.0, 0.0000),
                              (0.3200, 0.2582, 0.2582),
                              (0.4588, 0.2714, 0.2714),
                              (0.5490, 0.4719, 0.4719),
                              (0.6980, 0.7176, 0.7176),
                              (0.7882, 0.6500, 0.6500),
                              (0.8800, 0.6000, 0.6000),
                              #(0.9000, 0.5000, 0.5000),
                              #(0.9200, 0.4000, 0.4000),
                              #(0.9500, 0.2800, 0.2800),
                              #(0.9700, 0.1588, 0.1588),
                              (1.0000, 0.0000, 0.0000)),
                    'green': ((0.0, 0.0, 0.0000),
                              (0.1275, 0.2000, 0.2000),
                              (0.1498, 0.2593, 0.2593),
                              (0.1647, 0.3035, 0.3035),
                              (0.2078, 0.3841, 0.3841),
                              (0.2824, 0.5020, 0.5020),
                              (0.5216, 0.6397, 0.6397),
                              (0.6980, 0.7171, 0.7171),
                              (0.7882, 0.6892, 0.6892),
                              (0.7922, 0.6413, 0.6413),
                              (0.8000, 0.6447, 0.6447),
                              (0.8078, 0.6200, 0.6200),
                              (0.8157, 0.6000, 0.6000),
                              (0.8667, 0.5800, 0.5800),
                              (0.8745, 0.5500, 0.5500),
                              (0.8824, 0.5300, 0.5300),
                              #(0.8902, 0.5000, 0.5000),
                              #(0.8980, 0.4600, 0.4600),
                              #(0.9412, 0.4000, 0.4000),
                              #(0.9569, 0.3200, 0.3200),
                              #(0.9647, 0.2400, 0.2400),
                              #(0.9961, 0.1500, 0.1500),
                              #(0.9980, 0.1000, 0.1000),
                              (1.0000, 0.0800, 0.0800)),  
                    'blue' : ((0.0, 0.0, 0.0000),
                              (0.0150, 0.2584, 0.2584),
                              (0.0290, 0.5084, 0.5084),
                              (0.0780, 0.5512, 0.5512),
                              (0.1750, 0.5629, 0.5629),
                              (0.2140, 0.5349, 0.5349),
                              (0.3024, 0.5004, 0.5004),
                              (0.4667, 0.2748, 0.2748),
                              (0.5451, 0.3205, 0.3205),
                              (0.7843, 0.3561, 0.3561),
                              (0.8941, 0.3800, 0.3800),
                              #(0.9500, 0.4300, 0.4300),
                              (1.0000, 0.5000, 0.5000))}
cm_gistearthloop = _matplotlibcolorsLinearSegmentedColormap('loopingearth', _gist_earth_loop, 1024)
_additional_cm.update({'loopingearth':cm_gistearthloop})


_colormap_bwhite = { 'red'  :  (   (0., 0., 0.9),
                        (0.6, 0.45, 0.45),
                        (1., 0., 0.)),
          'green':  (   (0., 0., 0.9),
                        (0.6, 0.6, 0.6),
                        (1., 0., 0.)),
          'blue' :  (   (0., 0., 0.9),
                        (0.6, 0.8, 0.8),
                        (1., 0.05, 0.))}
cm_bwhite = _matplotlibcolorsLinearSegmentedColormap('bwhite', _colormap_bwhite, 1024)
_additional_cm.update({'cm_bwhite':cm_bwhite})



def colorbar(cmap="jet", cm_min=0, cm_max=1, arr=None):
    """
    cmap, norm, mappable = funcs.colorbar('jet', min, max)
    plt.scatter(x, y, c, cmap=cmap, norm=norm)
    cb = plt.colorbar(mappable)
    if arr is given, forces cm_min and cm_max to min-max of the arr
    """
    if cmap in _additional_cm.keys():
        cmap = _additional_cm[cmap]
    elif isinstance(cmap, str):
        cmap = _cm.get_cmap(cmap)
    if arr is not None: cm_min, cm_max = np.min(arr), np.max(arr)
    norm = _matplotlibpyplotNormalize(cm_min, cm_max)
    mappable = _cm.ScalarMappable(cmap=cmap, norm=norm)
    mappable._A = []
    return cmap, norm, mappable

