#!/bin/bash 

for j in `seq 1 10`
do echo $j
    #sbatch --export id=$j montagn.slurm
    #./montagn.slurm $j 2 100 'test'
    #montagn.slurm nlaunch usemodel nphot filename ndiffmax thetaobs dthetaobs force_wvl
    #sbatch montagn.slurm $j 14.0 100000 'mod14.0_2200nm_run8c' 10 90 5 2.2e-6
    #montagn.slurm nlaunch usemodel nphot filename ndiffmax dthetaobs force_wvl usethermal paramfile
    #python montagn_paral.py --nlaunch=$1 --path=$2 --paramfile=$3 --filename=$4 --nphot=$5 --usethermal=$6 --ndiffmax=$7 --cluster --force_wvl=$8  
    sbatch montagn.slurm $j '/scratch/lgrosset/' 'mod1.txt' 'mod1_1600nm_run1' 100000 0 10 1.6e-6
    #sbatch montagn.slurm $j '/scratch/lgrosset/' 'mod1.txt' 'mod1_1600nm_run1' 100000 0 10 'None'
done
